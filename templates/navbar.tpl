<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary mb-3">
    <a class="navbar-brand" href="">TODOList</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav w-100 d-flex justify-content-end">
            {if isset($username) && $username}
            <li class="nav-item active ml-0">
                <a class="nav-link" href="logout">{$username} Logout<span class="sr-only"></span></a>
            </li>
            {/if}
        </ul>
    </div>
</nav>