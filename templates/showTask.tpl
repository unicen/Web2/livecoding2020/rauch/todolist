{include 'header.tpl'}

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <h1>{$task->titulo}</h1>
            <p>{$task->descripcion}</p>
            <p> Prioridad:{$task->prioridad}</p>

            {if $task->finalizada == '1'}
                <p> La tarea esta finalizada </p>
            {else}
                <p> La tarea NO esta finalizada </p>
            {/if}
            {if !empty($task->imagen)}
                <img class="img-task" src="./{$task->imagen}" />
            {/if}

            <a href="{$base_url}listar">Volver</a>
        </div>

        <div class="col-md-3">
            {include 'vue/asideTasks.vue'}
        </div> 
    </div>

<script src="js/main.js"></script>
{include 'footer.tpl'}
    