{include 'header.tpl'}
    
<h1>{$titulo}</h1>

<!-- formulario de alta de tarea -->
<form action="nueva" method="post" class="my-4"  enctype="multipart/form-data">
    <div class="row">
        <div class="col-9">
            <div class="form-group">
                <label>Título</label>
                <input name="titulo" type="text" class="form-control">
            </div>
        </div>

        <div class="col-3">
            <div class="form-group">
                <label>Prioridad</label>
                <select name="prioridad" class="form-control">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>Descripcion</label>
        <textarea name="descripcion" class="form-control" rows="3"></textarea>
    </div>
    <input type="file" name="input_name" id="imageToUpload">

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>

<form method="post" action="finalizar">
    <ul class="list-group">

    {foreach $tareas item=tarea}
        
        <li class="list-group-item {if $tarea->finalizada}finalizada{/if}">
            <div class="row align-items-center">
                <div class="col-1">
                    {if !$tarea->finalizada}   
                        <input type="checkbox" name="finalizar_{$tarea->id_tarea}"/>
                    {/if}
                </div>
                <div class="col-3 titulo">
                    {$tarea->titulo} - {$tarea->descripcion}
                </div>
                <div class="col-2 offset-4 text-right">
                    {if !empty($tarea->imagen)}
                        <span class="badge badge-pill badge-secondary">IMG</span>
                    {/if} 
                    <span class="badge badge-pill badge-secondary">{$tarea->prioridad}</span>
                </div>  
                <div class="col-2 text-right">
                    <a class="btn btn-info" href="ver/{$tarea->id_tarea}">Ver</a>
                    <a class="btn btn-danger" href="eliminar/{$tarea->id_tarea}">Borrar</a>
                </div>            
            </div>
        </li>

    {/foreach}

    </ul>

    <input type="submit" class="btn btn-info mt-2" value="Finalizar seleccionadas">
</form>

{include 'footer.tpl'}      