<?php
require_once 'models/task.model.php';
require_once 'api/api.view.php';

class TaskApiController {

    private $model;
    private $view;
    private $data;

    public function __construct() {
        $this->model =  new TaskModel();
        $this->view = new APIView();
        
        $this->data = file_get_contents("php://input");
    }

    public function getTasks($params = []) {
        $orden = [];

        if (isset($_GET['sort'])) {
            $orden['sort'] = $_GET['sort'];
            if (isset($_GET['order'])) {
                $orden['order'] = $_GET['order'];
            }
        }        
        // obtengo las tareas
        $tareas = $this->model->getAll($orden);

        // se las paso a la vista para que responda JSON
        $this->view->response($tareas, 200);
    }

    public function getTask($params = []) {
        // obtengo el id de los params
        $idTarea = $params[':ID'];

        $colname = null;
        if (isset($params[':COLNAME'])) {
            $colname = $params[':COLNAME'];
        }

        $tarea = $this->model->get($idTarea, $colname);
        if ($tarea)
            $this->view->response($tarea, 200);
        else
            $this->view->response("no existe tarea con id {$idTarea}", 404);
    }

    public function deleteTask($params = []) {
        $idTarea = $params[':ID'];
        $tarea = $this->model->get($idTarea);
        
        // verifico que exista
        if (empty($tarea)) {
            $this->view->response("no existe tarea con id {$idTarea}", 404);
            die();
        }

        // si existe la elimina
        $this->model->delete($idTarea);
        $this->view->response("La tarea con id {$idTarea} se eliminó correctamente", 200);
    }

    public function getData() {
        return json_decode($this->data);
    }

    public function addTask() {
        // devuelve el objeto JSON enviado por POST     
        $body = $this->getData();

        // inserta la tarea
        $titulo = $body->titulo;
        $descripcion = $body->descripcion;
        $prioridad = $body->prioridad;
        
        $tarea_id = $this->model->insert($titulo, $descripcion, $prioridad, '');

        if ($tarea_id)
        {
            $this->view->response("Se agrego la tarea con id: {$tarea_id}", 200);
        }
        else
        {
            $this->view->response("La tarea no fue creada", 500);
        }
    }

    public function updateTask($params = []) {
        $task_id = $params[':ID'];
        $task = $this->model->get($task_id);

        if (!$task) {
            $this->view->response("Task id=$task_id not found", 404);
            die();
        }

        $body = $this->getData();
        $titulo = $body->titulo;
        $descripcion = $body->descripcion;
        $finalizada = $body->finalizada;
        $resultado = $this->model->update($task_id, $titulo, $descripcion, $finalizada);

        if ($resultado) {
            $this->view->response("Tarea id=$task_id actualizada con éxito", 200);
        }
        else 
        {
            $this->view->response("Error en actualizacion de tarea id=$task_id ", 500);
        }
            
    }

}