<?php
require_once 'views/auth.view.php';
require_once 'models/user.model.php';
require_once 'helpers/auth.helper.php';

class AuthController {

    private $view;
    private $model;

    public function __construct() {
        $this->view = new AuthView();
        $this->model = new UserModel();
    }

    public function showLogin() {
       $this->view->showFormLogin();
    }

    public function verify() {
        $username = $_POST['username'];
        $password = $_POST['password'];

        // busco el usuario
        $user = $this->model->getUser($username);

        // si existe el usuario y el password es correcto
        if ($user && password_verify($password, $user->password)) { 

            // abro session y guardo al usuario logueado
            AuthHelper::login($user);
            
            header("Location: " . BASE_URL . "listar");
        } else {
           $this->view->showFormLogin("Datos inválidos");
        }

    }

    /**
     * Destruye la session del usuario
     */
    public function logout() {
        AuthHelper::logout();
        header("Location: " . BASE_URL . 'login');
    }
}