<?php
require_once 'models/task.model.php';
require_once 'views/task.view.php';
require_once 'helpers/auth.helper.php';

class TaskController {

    private $model;
    private $view;

    public function __construct() {
        $this->model = new TaskModel();
        $this->view = new TaskView();
    }

    public function showTasks() {
        // barrera de seguridad
        AuthHelper::checkLogged();

        // pido las tareas al MODELO
        $tareas = $this->model->getAll();

        //var_dump($tareas); die();
        // actualizo la vista
        $this->view->showTasks($tareas);
    }

    public function viewTask($idTask) {
        // barrera de seguridad
        AuthHelper::checkLogged();

        $task = $this->model->get($idTask);

        if (!empty($task)) // verifico que exista la tarea
            $this->view->showTask($task);
        else
            $this->view->showError("La tarea no existe");
    }


    function addTask() {
        AuthHelper::checkLogged();

        // toma los valores enviados por el usuario
        $titulo = $_POST['titulo'];
        $descripcion = $_POST['descripcion'];
        $prioridad = $_POST['prioridad'];
    
        // verifica los datos obligatorios
        if (empty($titulo) || empty($prioridad)) {
            $this->view->showError("Faltan datos obligatorios");
            die();
        }

        // control de prioridad
        if ($prioridad == 5) {
            $tareas5 = $this->model->getAllByPrioridad(5); // devuelve un arreglo con las tareas con prioridad 5
            if (!empty($tareas5)) { // si hay tareas con prioridad 5 muestro error y freno
                $this->view->showError("No puede haber dos tareas con prioridad 5");
                die();
            }
        }

        if (($_FILES['input_name']['type'] == "image/jpg" || 
        $_FILES['input_name']['type'] == "image/jpeg" || 
        $_FILES['input_name']['type'] == "image/png")) {
            $success = $this->model->insertConImagen($titulo, $descripcion, $prioridad);
        } else {
            $success = $this->model->insert($titulo, $descripcion, $prioridad);
        }

        if($success) {
            header('Location: ' . BASE_URL . "listar");
        }
        else
            $this->view->showError("No se pudo insertar la tarea");

    }

    public function deleteTask($idTask) {
        AuthHelper::checkLogged();

        $this->model->delete($idTask);
        header('Location: ' . BASE_URL . "listar"); 
    }

    public function actualizarImagenes() {

        foreach ($_FILES as $index => $file) {
            $size = $file['size'];
            if ($size > 0 && 
            ($_FILES['input_name']['type'] == "image/jpg" || 
            $_FILES['input_name']['type'] == "image/jpeg" || 
            $_FILES['input_name']['type'] == "image/png")) {
                // Obtengo el id
                $id = explode('_', $index)[2];
                $this->modelo->actualizarArchivo($id, $file);
            }
        }
    }

    public function actualizarEstados() {
        // Array ( [finalizar_23] => on [finalizar_25] => on [finalizar_26] => on ) 
        foreach ($_POST as $index => $elemento) {
            $vars = explode('_', $index);

            // $vars[0] = 'finalizar', $var[1] = 23
            $idTask = $vars[1];
            $this->model->finalize($idTask);
        }

    }

    function finalizeTasks() {
        AuthHelper::checkLogged();

        $this->actualizarEstados();

        header('Location: ' . BASE_URL . "listar"); 
    }

    function showError($error) {
        $this->view->showError($error); 
    }

    

}