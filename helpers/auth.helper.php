<?php
    /**
     * Helper de autenticacion
     * 
     */
    class AuthHelper {

        /**
         * Inicia la session solo si no está iniciada.
         * 
         * Corrige el problema de "session ya iniciada".
         */
        static private function start() {
            if (session_status() != PHP_SESSION_ACTIVE)
                session_start();
        }

        /**
         * Verifica que exista un usuario logueado. 
         * Si no esta logueado lo redirige al login y corta la ejecución.
         */
        static public function checkLogged() {
            // llama al metodo estatico privado para iniciar la session
            self::start(); 

            if (!isset($_SESSION['IS_LOGGED'])) {
                header('Location: ' . BASE_URL . 'login');
                die();
            }
        }

        /**
         * Determina si el usuario esta logueado
         */
        static public function isLogged() {
            self::start(); 
            return (isset($_SESSION['IS_LOGGED']));
        }

        /**
         * Devuelve el username del usuario logueado
         */
        static public function getUserLogged() {
            self::start();
            if (isset($_SESSION['USERNAME'])) {
                return $_SESSION['USERNAME'];
            }
            
            return false;
        }


        /**
         * Inicia la session y guarda los datos dal usuario
         */
        static public function login($user) {
            // INICIO LA SESSION Y LOGUEO AL USUARIO
            self::start();
            $_SESSION['IS_LOGGED'] = true;
            $_SESSION['ID_USER'] = $user->id_usuario;
            $_SESSION['USERNAME'] = $user->username;
        }

        /**
         * Destruye la session
         */
        static public function logout() {
            self::start();
            session_destroy();
        }


    }