<?php
/**
 * Clase principal de modelo de la que 
 * deben extender todos los modelos
 */
class Model {

    private $db;

    public function __construct() {
        // 1. abro la conexión con MySQL 
        $this->db = $this->createConection();
    }

    public function getDb()
    {
        return $this->db;
    }

    /**
     * Crea la conexion
     */
    public function createConection() {
        $host = 'localhost'; // localhost
        $userName = 'root'; // casi siempre es root
        $password = ''; // puede ser vacio
        $database = 'db_tareas'; // nombre de la base de datos

        try {
            $pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $userName, $password);
            
            // solo en modo desarrollo
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $pdo;
    }

}