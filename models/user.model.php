<?php
require_once ('base.model.php');

class UserModel extends Model {

    public function getUser($username) {
        
        $sentencia = $this->getDb()->prepare("SELECT * FROM usuarios WHERE username = ?");
        $sentencia->execute([$username]);
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }

}