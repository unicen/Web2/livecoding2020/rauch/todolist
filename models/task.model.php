<?php
require_once ('base.model.php');

/**
 * Interactua con la tabla tareas
 */
class TaskModel extends Model {

    function white_list(&$value, $allowed, $message) {
        if ($value === null) {
            return $allowed[0];
        }
        $key = array_search($value, $allowed, true);
        if ($key === false) { 
            throw new InvalidArgumentException($message); 
        } else {
            return $value;
        }
    }

    /**
     * Devuelve todas las tareas.
     */
    public function getAll($orden = []) {
        $order = 'finalizada';
        $direction = 'ASC';

        if (isset($orden['sort'])) {
            $order = $orden['sort'];
            if (isset($orden['order'])) {
                $direction = $orden['order'];
            }
        }

        $order = $this->white_list($order, ["id_tarea", "finalizada", "titulo","descripcion","prioridad"], "Columna no valida");
        $direction = $this->white_list($direction, ["ASC","DESC"], "Direccion de ORDER BY no valida");

        $sql = "SELECT * FROM tareas ORDER BY $order $direction";

        // 2. enviamos la consulta (3 pasos)
        $sentencia = $this->getDb()->prepare($sql); // prepara la consulta
        $sentencia->execute(); // ejecuta
        $tareas = $sentencia->fetchAll(PDO::FETCH_OBJ); // obtiene la respuesta

        return $tareas;
    }

    /**
     * Devuelve una tarea con el id determinado
     */
    public function get($idTask, $colname = null)
    {
        if (!isset($colname)) {
            $colname = "*";
        };

        $colname = $this->white_list($colname, ["*", "id_tarea", "finalizada", "titulo","descripcion","prioridad"], "Columna no valida");

        $sql = "SELECT $colname FROM tareas WHERE id_tarea = ?";

        // 2. enviamos la consulta (3 pasos)
        $sentencia = $this->getDb()->prepare($sql); // prepara la consulta
        $sentencia->execute([$idTask]); // ejecuta
        $tarea = $sentencia->fetch(PDO::FETCH_OBJ); // obtiene la respuesta

        return $tarea;
    }

    /**
     * Devuelve todas las tareas con la prioridad indicada
     */
    public function getAllByPrioridad($prioriodad) {
        // 2. enviamos la consulta (3 pasos)
        $sentencia = $this->getDb()->prepare("SELECT * FROM tareas WHERE prioridad = ?"); // prepara la consulta
        $sentencia->execute([$prioriodad]); // ejecuta
        $tareas = $sentencia->fetchAll(PDO::FETCH_OBJ); // obtiene la respuesta

        return $tareas;
    }

    /**
     * Mueve el archivo subido y le asigna un nombre,
     * retorna el nombre creado.
     */
    public function copiarImagen()
    {
        // Nombre archivo original
        $nombreOriginal = $_FILES['input_name']['name'];
        // Nombre en el file system:
        $nombreFisico = $_FILES['input_name']['tmp_name'];
        
        $nombreFinal = "images/". uniqid("", true) . "." 
        . strtolower(pathinfo($nombreOriginal, PATHINFO_EXTENSION));

        move_uploaded_file($nombreFisico, $nombreFinal); 
        
        return $nombreFinal;
    }

    public function insertConImagen($titulo, $descripcion, $prioridad)
    {
        $nombreFinal = $this->copiarImagen();
        $success = $this->insert($titulo, $descripcion, $prioridad, $nombreFinal);
        return $success;
    }

    /**
     * Inserta una tarea.
     */
    public function insert($titulo, $descripcion, $prioridad, $imagen = null) {
        $sentencia = $this->getDb()->prepare("INSERT INTO tareas(titulo, descripcion, prioridad, finalizada, imagen) VALUES(?, ?, ?, ?, ?)"); // prepara la consulta
        $sentencia->execute([$titulo, $descripcion, $prioridad, 0, $imagen]);

        $lastId = $this->getDb()->lastInsertId();

        return  $lastId;  
    }

    public function finalize($idTask) {
       $sentencia = $this->getDb()->prepare("UPDATE tareas SET finalizada = 1 WHERE id_tarea = ?"); // prepara la consulta
       return $sentencia->execute([$idTask]); // ejecuta    
    }

    /**
     * Borra una tarea
     */
    public function delete($idTask) {
        // 2. enviamos la consulta
        $sentencia = $this->getDb()->prepare("DELETE FROM tareas WHERE id_tarea = ?"); // prepara la consulta
        $sentencia->execute([$idTask]); // ejecuta    
    }
        
    public function update($task_id, $titulo, $descripcion, $finalizad) {
        $sql = "UPDATE tareas
                SET titulo = ?, descripcion = ?, finalizada = ?
                WHERE id_tarea = ?";

        $sentencia = $this->getDb()->prepare($sql); 
        $result = $sentencia->execute([$titulo, $descripcion, $finalizad, $task_id]); // ejecuta    

        return $result;
    }
}