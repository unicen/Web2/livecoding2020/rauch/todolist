<?php
require_once('base.view.php');
require_once('helpers/auth.helper.php');

class TaskView extends View {

    public function __construct() {
        parent::__construct();

        $userName = AuthHelper::getUserLogged();
        $this->getSmarty()->assign('username', $userName);
    }


    public function showTasks($tareas) {
        $this->getSmarty()->assign('tareas', $tareas);
        $this->getSmarty()->assign('titulo', 'Lista de tareas');
        
        $this->getSmarty()->display('showTasks.tpl');
    }

    public function showTask($task) {
        $this->getSmarty()->assign('task', $task);
        $this->getSmarty()->display('showTask.tpl');
    }

    /**
     * Muestra errores por pantalla
     */
    public function showError($msg) {
        $this->getSmarty()->assign('msg', $msg);
        $this->getSmarty()->display('showError.tpl');
    }
}