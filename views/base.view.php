<?php
require_once('libs/smarty/Smarty.class.php');


/**
 * Clase padre View de la que extienden todas las Views
 */
class View  {

    private $smarty;

    public function __construct() {
        $this->smarty = new Smarty();
        $this->smarty->assign('base_url', BASE_URL);

    }    

    public function getSmarty() {

        return $this->smarty;
        
    }
}