<?php
require_once('base.view.php');

class AuthView extends View {

    /**
     * Muestra el formulario de login
     * 
     * $error si se envia el parametro opcional muestra el error por pantalla
     */
    public function showFormLogin($error = null) {

        $this->getSmarty()->assign('error', $error);
        $this->getSmarty()->display('formLogin.tpl');
    }
}